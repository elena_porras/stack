# Stack
Stack is a nimble development helper. Stack wraps around Docker and helps you set up your development environment quickly and easily.

### Installation

To install Stack, run ./install.sh if you are on Linux or Mac. Windows user have to figure it out themselves. Sorry.

# Usage
Run `stack` to see the help screen.

- `stack install` installs the containers,
- `stack up` runs the containers, and
- `stack down` stops the containers.
- `stack compose` regenerates the docker-compose.yml file without you needing to run `stack install`.

# Configuration
The configuration `stack.yml` file is a YAML file defining the Docker settings for `docker-compose.yml`, installation, running and stopping instructions for your Docker containers.

### Overview
`stack.yml` has the following format:

```yaml
services:
  service_name:
    disabled: true
    install:
      pre:
        # - instruction
      post:
        # - instruction
    up:
      pre:
        # - instruction
      post:
        # - instruction
    down:
      pre:
        # - instruction
      post:
        # - instruction
    docker:
      # your settings for generated docker-compose.yml
docker:
  # for this section read 'Adding independent Docker Compose directives' below
```

### Service

Every service is placed right under `services` and defined as `service_name`. Realize, that `service_name` in the
placeholder is just an example and you may supply any valid name. The name is then used as the container name in
 `docker-compose.yml`.
 
Every service should at least define a `docker` section, describing whatever that you wish to be put into
`docker-compose.yml`.

If you wish for additional instructions during installation, running and stopping containers, you should define
`install`, `up` and `down` sections under service name. The instructions are described further below.

### Disabling a service

You may choose to disable a service by setting `disabled: true` under `service_name`.
If you set `disabled` to anything, but `true`, `'true'`, `'True'` or `'TRUE'` or even completely remove it, it will always
evaluate to `False` and the service will be installed, brought up and brought down. 

### Instructions

As mentioned above, you may supply additional instructions in `install`, `up` and `down` sections under service name.

For `install` and `up`, **`pre`** indicates the set of instructions needs to be run prior to container start.

**`post`**, logically, indicates the opposite (for `install` and `up`) - the set of instructions needs to be run after
container starts.

While `pre` instruction are pretty straightforward - it is a simple sequence of scalars - `post` instructions must be
defined as a sequence of mappings:

```yaml
      pre:
        - git pull
        - echo "all done"
      post:
        -
          location: inside
          user: nginx
          instruction: /html/bin/console
        -
          location: outside
          instruction: echo "all done"
```

The reason for this is simple: while `pre` is executed while containers are down and there is no chance to run something
in a container, `post` is executed while containers are up, and you may want to run certain instructions inside the
container and some outside of it.

For each mapping in the sequence of mappings, you must define at least `location` and `instruction`. You may omit `user`
when defining an instruction inside container - this leads to defaulting `user` to `root`.

For `down` the opposite is in effect: `pre` works as `post` and vice versa.

### Custom variables
You may specify custom variables in your `stack.yml`. Custom variables are placed into instructions between double `%%`:

```yaml
    up:
      pre:
        - echo %%hello%%
```

Now, whenever you run `stack up`, you will have to specify the variable for `%%hello%%`, otherwise stack will stop with
a fatal error:

```
[example@localhost ~]$ stack up
FATAL: You did not specify the required variables.
[example@localhost ~]$ stack up HELLO
HELLO
# the rest of the up process
```

### Adding independent Docker Compose directives

It is possible to add more directives for to-be generated `docker-compose.yml` file, i.e. `volumes` or `networks`, which
 usually go under `services`. To do so, you have to specify `docker` directive in `stack.yml` like so:

```yaml
services:
  service_name:
    # service
  another_service_name:
    # service
docker:
  volumes: # example
    nfsmount: # example
      driver: local # example
      driver_opts: # example
        # and so on
```

### Overriding Docker Compose version
The default version of generated `docker-compose.yml` file is `3.7`. However, it is possible to override it.

For that, define `version: <version>` under `docker`, like so:

 ```yaml
services:
  service_name:
    # service
  another_service_name:
    # service
docker:
  version: '2' 
```

### How we use it
Stack was born as an automation tool for all of our environments - development, staging and testing.

In testing, we parse the branch name from what our repo sends and execute the required sequence of steps.

Something similar happens on staging as well.

And on development, Stack is a great tool in combination with D to keep the devs updated and help them write more code
 and do all the side-effect non-coding routines less.

# Example
Explore [`stack-example`](https://github.com/flexbox-it/stack-examples/tree/symfony4) repository to see how to quickly
set up Symfony 4 Demo app.
