from distutils.core import setup

setup(
    name='stack',
    version='0.1.0',
    packages=['stack'],
    package_data={'stack': ['docker-compose.yml']},
    entry_points={
        'console_scripts': [
            'stack = stack.__main__:main'
        ]
    },
    install_requires=['PyYAML']
)
