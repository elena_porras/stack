import io
import os
import re
import sys
import yaml


class Stack(object):
    docker_compose_file_awd = ''
    docker_compose_file_cwd = ''
    service_config_file_cwd = ''
    supported_config_version = 1.0
    settings = {}

    def __init__(self):
        cwd = os.getcwd()
        self.service_config_file_cwd = cwd + "/stack.yml"
        self.docker_compose_file_cwd = cwd + "/docker-compose.yml"
        self.docker_compose_file_awd = os.path.dirname(__file__) + "/docker-compose.yml"
        self.settings = self._read_file(self.service_config_file_cwd)
        self._revise_settings_version()

    def _read_file(self, file_name):
        result = {}

        try:
            with open(file_name, "r") as stream:
                try:
                    result = yaml.load(stream, Loader=yaml.SafeLoader)
                except yaml.YAMLError as ex:
                    print(ex)
                    sys.exit()
        except FileNotFoundError:
            print("Fatal: File " + file_name + " not found.")
            sys.exit()

        return result

    def _revise_settings_version(self):
        if self.supported_config_version < self.settings['version']:
            print("FATAL: stack.yml version ({}) is not consistent with supported config version ({}).".format(
                self.settings['version'], self.supported_config_version))
            sys.exit()

    def _is_service_disabled(self, service_name):
        try:
            return self.settings['services'][service_name]['disabled'] in ['true', 'True', 'TRUE', True]
        except KeyError:
            return False
        sys.exit()

    def generate_docker_compose_file(self):
        compose_template = self._read_file(self.docker_compose_file_awd)

        try:
            for service_name in self.settings['services']:
                if not self._is_service_disabled(service_name):
                    compose_template['services'][service_name] = self.settings['services'][service_name]['docker']
                    compose_template['services'][service_name]['container_name'] = service_name
        except KeyError:
            print("FATAL: stack.yml is malformed!")
            sys.exit()
        try:
            compose_template.update(self.settings['docker'])
            compose_template['version'] = self.settings['docker']['version']
        except KeyError:
            #If self.settings['docker'] (i.e. `docker` in stack.yml) is not defined, just ignore it and proceed
            pass
        with io.open(self.docker_compose_file_cwd, "w", encoding="utf8") as output:
            yaml.dump(compose_template, output, default_flow_style=False, allow_unicode=True)

    def _start_containers(self):
        os.system("docker-compose -f " + self.docker_compose_file_cwd + " up -d")

    def _stop_containers(self):
        os.system("docker-compose -f " + self.docker_compose_file_cwd + " down")

    def _determine_user_for_instruction(self, instruction_set):
        user = ""

        try:
            if len(instruction_set['user']) > 0:
                user = instruction_set['user']
            else:
                user = "root"
        except IndexError:
            user = "root"
        except TypeError:
            user = "root"

        return user

    def _run_instructions(self, iterator, where, when):
        def replace_match(match):
            return next(iterator)

        def run_string_command():
            try:
                for service_name in self.settings['services']:
                    if not self._is_service_disabled(service_name):
                        for instruction in self.settings['services'][service_name][where][when]:
                            instruction = re.sub(r"%%([a-z_])*%%", replace_match, instruction)
                            os.system(instruction)
            except StopIteration:
                print('FATAL: You did not specify the required variables.')
                sys.exit()
            except KeyError:
                pass

        def run_sequence_of_commands():
            try:
                for service_name in self.settings['services']:
                    if not self._is_service_disabled(service_name):
                        for instruction_set in self.settings['services'][service_name][where][when]:
                            try:
                                instruction = instruction_set['instruction']
                                instruction = re.sub(r"%%([a-z_])*%%", replace_match, instruction)

                                if "inside" == instruction_set['location']:
                                    user = self._determine_user_for_instruction(instruction_set)
                                    instruction = "docker exec -i -u " + user + " " + service_name + " " \
                                                  + instruction
                                os.system(instruction)
                            except StopIteration:
                                print('FATAL: You did not specify the required variables.')
                                sys.exit()
                            except KeyError as ex:
                                print(ex)
                                sys.exit()
            except KeyError:
                pass

        if where in ('install', 'up'):
            if 'pre' == when:
                run_string_command()
            elif 'post' == when:
                run_sequence_of_commands()
            else:
                print("FATAL: Wrong 'when' argument")
                sys.exit()
        elif 'down' == where:
            if 'pre' == when:
                run_sequence_of_commands()
            elif 'post' == when:
                run_string_command()
            else:
                print("FATAL: Wrong 'when' argument")
                sys.exit()
        else:
            print("FATAL: Wrong 'where' argument")
            sys.exit()

    def _initialize_iterator(self):
        return iter(sys.argv[2::])

    def install(self):
        iterator = self._initialize_iterator()

        self.generate_docker_compose_file()
        self._run_instructions(iterator, 'install', 'pre')
        self._start_containers()
        self._run_instructions(iterator, 'install', 'post')

    def up(self):
        iterator = self._initialize_iterator()

        self._run_instructions(iterator, 'up', 'pre')
        self._start_containers()
        self._run_instructions(iterator, 'up', 'post')

    def down(self):
        iterator = self._initialize_iterator()

        self._run_instructions(iterator, 'down', 'pre')
        self._stop_containers()
        self._run_instructions(iterator, 'down', 'post')
