import sys
from stack.helper import print_helper
from stack.Stack import Stack


def main():
    try:
        if sys.argv[1] in ('install', 'up', 'down', 'compose'):
            stack = Stack()

            if 'install' == sys.argv[1]:
                stack.install()
            elif 'up' == sys.argv[1]:
                stack.up()
            elif 'down' == sys.argv[1]:
                stack.down()
            elif 'compose' == sys.argv[1]:
                stack.generate_docker_compose_file()
        else:
            print_helper()
    except IndexError:
        print_helper()


if __name__ == '__main__':
    main()