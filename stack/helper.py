def print_helper():
    bold_start = "\033[1m"
    bold_end = "\033[0;0m"

    print("""
     $$$$$$\    $$\                         $$\\
    $$  __$$\   $$ |                        $$ |
    $$ /  \__|$$$$$$\    $$$$$$\   $$$$$$$\ $$ |  $$\\
    \$$$$$$\  \_$$  _|   \____$$\ $$  _____|$$ | $$  |
     \____$$\   $$ |     $$$$$$$ |$$ /      $$$$$$  /
    $$\   $$ |  $$ |$$\ $$  __$$ |$$ |      $$  _$$<
    \$$$$$$  |  \$$$$  |\$$$$$$$ |\$$$$$$$\ $$ | \$$\\
     \______/    \____/  \_______| \_______|\__|  \__|

    """ + bold_start + """DESCRIPTION:""" + bold_end + """
      Stack - Quick Docker project creator. 

      Stack uses stack.yml file as it's handbook to install, run and stop containers.

    """ + bold_start + """USAGE:""" + bold_end + """
      """ + bold_start + """stack <command>""" + bold_end + """

    """ + bold_start + """COMMANDS:""" + bold_end + """
      """ + bold_start + """install""" + bold_end + """
        Install containers.

      """ + bold_start + """up""" + bold_end + """
        Run Docker containers.

      """ + bold_start + """down""" + bold_end + """
        Stop Docker containers.

      """ + bold_start + """compose""" + bold_end + """
        Regenerate docker-compose.yml.
    """)
