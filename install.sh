#!/usr/bin/env bash

valid_python_versions=("3.6" "3.7")
python_version=`python3 -c "import sys;t='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]));sys.stdout.write(t)";`

if [[ ! " ${valid_python_versions[@]} " =~ " ${python_version} " ]]; then
    echo "Only the following versions of Python are supported: "
    echo $(IFS=, ; echo "${valid_python_versions[*]}")
    exit 1
fi

uname="$(uname -s)"

case "${uname}" in
  Linux*)
    pip3 install --user --force-reinstall --upgrade .
    ;;
  Darwin*)
    pip3 install --user --force-reinstall --upgrade .
    sudo ln -s ~/Library/Python/"${python_version}"/bin/stack /usr/local/bin/stack

    echo "Your python installation directory might be root only."
    echo "If Stack does not work on your system, try reassigning the python installation directory to your user"
    echo "by running 'sudo chown $USER ~/Library/Python'"
    ;;
  *)
    echo "FATAL: Could not figure out your OS. It doesn't look like Linux or MacOS."
    ;;
esac

